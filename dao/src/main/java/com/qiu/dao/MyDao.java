package com.qiu.dao;

import com.qiu.bean.Score;
import com.qiu.bean.Stu;
import com.qiu.bean.Users;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface MyDao {
    Users login(Users users);
    List list(Map map);
    int getBySnowId(@Param("snowId")String snowId);
    int addStu(@Param("snowId")String snowId,@Param("id") Integer id);
    int  addScore(Score score);
    int delScore(Integer cid);
    List slist(@Param("sid")String sid);
    List cla();
    int updStu(Stu stu);
    int updScre(Score score);
    Map getByCid(Integer cid);
}
