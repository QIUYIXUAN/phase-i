package com.qiu.service;

import com.qiu.bean.Score;
import com.qiu.bean.Stu;
import com.qiu.bean.Users;
import com.qiu.dao.MyDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
@Service
public class MyServiceImpl implements MyService {
  @Resource
  private MyDao myDao;
    @Override
    public Users login(Users users) {
        return myDao.login(users);
    }

    @Override
    public List list(Map map) {
        return myDao.list(map);
    }

    @Override
    public int getBySnowId(String snowId) {
        return myDao.getBySnowId(snowId);
    }

    @Override
    public int addStu(String snowId, Integer id) {
        return myDao.addStu(snowId,id);
    }

    @Override
    public int addScore(Score score) {
        return myDao.addScore(score);
    }

    @Override
    public int delScore(Integer cid) {
        return myDao.delScore(cid);
    }

    @Override
    public List slist(String sid) {
        return myDao.slist(sid);
    }

    @Override
    public List cla() {
        return myDao.cla();
    }

    @Override
    public int updStu(Stu stu) {
        return myDao.updStu(stu);
    }

    @Override
    public int updScre(Score score) {
        return myDao.updScre(score);
    }

    @Override
    public Map getByCid(Integer cid) {
        return myDao.getByCid(cid);
    }
}
