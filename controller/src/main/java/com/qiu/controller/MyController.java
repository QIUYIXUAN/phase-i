package com.qiu.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qiu.bean.Score;
import com.qiu.bean.Stu;
import com.qiu.bean.Users;
import com.qiu.service.MyService;
import com.qiu.untils.FileUploadUtil;
import com.qiu.untils.SnowflakeIdUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MyController {
    private String snowId="";
    @Resource
    private MyService myService;
    @RequestMapping("login.do")
    public String login(Users users, HttpSession session){
        Users login = myService.login(users);
        if (login!=null){
            session.setAttribute("users",login);
            return "redirect:list.do";
        }else {
            return "redirect:index,jsp";
        }
    }
    @RequestMapping("list.do")
    public String list(@RequestParam(defaultValue = "1",value = "vapge")Integer vapge,Integer id,String mohu ,Model model){
        Map map = new HashMap();
        map.put("mohu",mohu);
        map.put("id",id);
        PageHelper.startPage(vapge,3);
        List list = myService.list(map);
        PageInfo p = new PageInfo(list);
        model.addAttribute("p",p);
        model.addAttribute("map",map);
        return "list";
    }
@RequestMapping("cla.do")
    @ResponseBody
    public Object cla(){
        return myService.cla();
}
@RequestMapping("getBySnowId.do")
@ResponseBody
    public Object getBySnowId(){
        if (snowId.length()==0||myService.getBySnowId(snowId)>0){
            long l = new SnowflakeIdUtils(1, 3).nextId();
            snowId=l+"";
        }
        return snowId;
}
@RequestMapping("slist.do")
    @ResponseBody
    public Object slist(String sid){
    List list = myService.slist(sid);
    return list;
}
@RequestMapping("toadd.do")
    public String toadd(String snowId){
        return "add";
}

@RequestMapping("addcore.do")
    @ResponseBody
    public Object addScore(Score score,Integer id){
    int i = myService.getBySnowId(score.getSid());
    if (i==0&&score.getSid().length()>0){
        snowId=score.getSid();
        myService.addStu(snowId,id);
    }
    int i1 = myService.addScore(score);
    if (i1>0){
        return snowId;
    }else {
        return null;
    }

}
@RequestMapping("upd.do")
    @ResponseBody
public Object upd(Stu stu, HttpSession session, MultipartFile photo) {
    System.out.println("-----------------"+stu);
     if (photo.getSize()>0){
         String upload = FileUploadUtil.load(session, photo, "upload");
         stu.setPic(upload);
     }
     return myService.updStu(stu);

}
@RequestMapping("del.do")
    @ResponseBody
    public Object del(Integer cid){
        return myService.delScore(cid);
}
@RequestMapping("toupd2.do")
public String toupd2(){
        return "upd";
}
@RequestMapping("upd2.do")
    @ResponseBody
    public Object upd2(Score score){
        return myService.updScre(score);
}
@RequestMapping("getByCid.do")
@ResponseBody
    public Object getByCid(Integer cid){
        return myService.getByCid(cid);
}
}

