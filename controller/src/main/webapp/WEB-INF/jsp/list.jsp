<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String ctx = request.getContextPath();
    request.setAttribute("ctx", ctx);
%>
<html>
<head>
    <title>Title</title>
    <script src="js/fileupload.js"></script>
    <script src="js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="css/index_work.css">
    <link rel="stylesheet" href="css/img.css">
    <script src="My97DatePicker/WdatePicker.js"></script>
<script src="/bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap-4.6.0-dist/css/bootstrap.min.css">
</head>
<body>
<div class="row">
    <div class="col-2">
        <jsp:include page="/lay/left1.jsp"></jsp:include>
    </div>
    <div class="col-10">
        <h1>列表模糊</h1>
        <form method="post" action="list.do">
            <input type="hidden" name="vapge">
        </form>
        <table class="table table-bordered table-hover table-info">
            <tr>
                <td>学生编号</td>
                <td>学生姓名</td>
                <td>登记Id</td>
                <td>登记名字</td>
                <td>总成绩</td>
                <td>报考科目</td>
                <td>平均成绩</td>
                <td>上传文件路径</td>
                <td>班级名称</td>
                <td><input type="button" value="添加" onclick="toadd(${param.id})"></td>
            </tr>
            <c:forEach items="${p.list}" var="s">
                <tr>
                    <td>${s.sid}</td>
                    <td>${s.sname}</td>
                    <td>${s.lid}</td>
                    <td>${s.lname}</td>
                    <td>${s.tosum}</td>
                    <td>${s.tonum}</td>
                    <td>${s.toavg}</td>
                    <td>${s.pic}</td>
                    <td>${s.name}</td>
                </tr>
            </c:forEach>
            <tr>
                <td colspan="20">
                    <input type="button" value="首页" onclick="fenye(1)">
                    <input type="button" value="上一页" onclick="fenye(${p.isFirstPage?1:p.prePage})">
                    <input type="button" value="下一页" onclick="fenye(${p.isLastPage?p.pages:p.nextPage})">
                    <input type="button" value="尾页" onclick="fenye(${p.pages})">
                </td>
            </tr>
        </table>
    </div>
</div>
<script>
   function fenye(vapge){
       $("[name='vapge']").val(vapge);
       $("form").submit();
   }
   function toadd(id) {
  location="toadd.do?id="+id;
   }
</script>
</body>
</html>
