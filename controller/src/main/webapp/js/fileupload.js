function contentLoaded() {
    $("[name='photo']").change(function (){
        var flag=$(this)
        var file = $(this).get(0).files[0];
        if(file)
        {
            var reader = new FileReader();
            reader.onload = function(event) {
                var txt = event.target.result;
                var img = document.createElement("img");
                img.src = txt; //将图片base64字符串赋值给img的src
                flag.parent().children("img").prop("src", txt);
            };
        }
        reader.readAsDataURL(file);
    })
}
window.addEventListener("DOMContentLoaded", contentLoaded, false);