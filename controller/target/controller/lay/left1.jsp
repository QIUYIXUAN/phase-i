<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String ctx = request.getContextPath();
    request.setAttribute("ctx", ctx);
%>
<html>
<head>
    <title>Title</title>
    <script src="../js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="../bootstrap-4.6.0-dist/css/bootstrap.min.css">

    <link rel="stylesheet" href="../font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="../bootstrap-4.6.0-dist/css/bootstrap.min.css">
    <link href="https://cdn.bootcss.com/bootstrap-table/1.11.1/bootstrap-table.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.bootcss.com/jquery-treegrid/0.2.0/css/jquery.treegrid.min.css">
    <script src="https://cdn.bootcss.com/bootstrap-table/1.12.1/bootstrap-table.min.js"></script>
    <script src="https://cdn.bootcss.com/bootstrap-table/1.12.0/extensions/treegrid/bootstrap-table-treegrid.js"></script>
    <script src="https://cdn.bootcss.com/jquery-treegrid/0.2.0/js/jquery.treegrid.min.js"></script>
</head>
<body>
    <div class="container">
        <div>
            欢迎${user.uname}登录
        </div>
        <table id="tb_hq"></table>
    </div>
    <script type="text/javascript">
             var $table_hq = $('#tb_hq');
                var data1=new Array();
                $.ajax({
                    url:"cla.do",
                    type:"post",
                    dataType:"json",
                    async:false,
                    success:function (obj){
                        for (let x in obj) {
                            if(obj[x].flag=="item"){
                                obj[x].name="<a href='list.do?id="+obj[x].id+"' >"+obj[x].name+"</a>"
                            }
                            data1.push(obj[x])
                        }
                    }
                })

                 $table_hq.bootstrapTable({
                     data: data1,
                     idField: 'id',
                     dataType: 'json',
                     columns: [{
                             field: 'name',
                             width: 140
                         } ],
                     //在哪一列展开树形
                     treeShowField: 'name',
                     //指定父id列
                     parentIdField: 'pid',
                     onResetView: function(data) { //onResetView  当切换视图模式时 触发此事件
                         $table_hq.treegrid({
                             // initialState: 'collapsed', // 所有节点都折叠
                              initialState: 'expanded',// 所有节点都展开，默认展开
                             // treeColumn: 0,  // 树中表格的哪一列
                             //expanderExpandedClass: 'glyphicon glyphicon-minus',  //图标样式
                             //expanderCollapsedClass: 'glyphicon glyphicon-plus',
                             expanderExpandedClass: 'fa fa-minus-square-o',
                             expanderCollapsedClass: 'fa fa-plus-square-o',
                             onChange: function() {
                                 $table_hq.bootstrapTable();
                             }
                         });
                         //只展开树形的第一级节点
                         //$table_hq.treegrid('getRootNodes').treegrid('expand');
                     },
                 });
         </script>
</body>
</html>
