<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String ctx = request.getContextPath();
    request.setAttribute("ctx", ctx);
%>
<html>
<head>
    <title>Title</title>
    <script src="js/fileupload.js"></script>
    <script src="js/jquery-1.9.1.min.js"></script>
    <link rel="stylesheet" href="css/index_work.css">
    <link rel="stylesheet" href="css/img.css">
    <script src="My97DatePicker/WdatePicker.js"></script>
    <script src="/bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/bootstrap-4.6.0-dist/css/bootstrap.min.css">
</head>
<body>
<div class="row">
    <div class="col-md-3"><jsp:include page="/lay/left1.jsp"></jsp:include></div>
    <div class="col-md-9">
        <form method="post" id="f1">
            <input type="hidden" name="id" value="${param.id}">
            学生Id：<input type="text" name="sid" value="${param.snowId!=""?param.snowId:snowId}" readonly="readonly">
            学生姓名：<input type="text" name="snamr">
            登记人ID：<input type="text" name="lid">
            登记人：<input type="text" name="lname">
            总成绩：<input type="text" name="tosum">
            报考科目:<input type="text" name="tonum">
            平均成绩：<input type="text" name="toavg">
            上传文件路径<input type="file" name="pic">
        </form>
        <!-- Button trigger modal -->
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            添加科目成绩
        </button>
          <table class="table table-hover table-hover table-info" id="tab">
              <tr>
                  <td>科目</td>
                  <td>成绩</td>
              </tr>
          </table>
        <button type="button" class="btn[ btn-success" onclick="addStu()">添加成绩表</button>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                  <form id="fom" method="post">
                      <input type="hidden" value="${param.id}" name="id">
                      <input type="text" name="sid" value="${param.snowId!=""?param.snowId:snowId}">
                      科目:<input type="text" name="cname">
                      成绩:<input type="text" name="score">
                  </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">返回</button>
                        <button type="button" class="btn btn-primary" onclick="addSocre()">提交</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
let sid = $("[name='sid']").val();
         $.ajax({
                         url:"getBySnowId.do",
                         type:"post",
                         dataType:"text",
                         success:function(obj) {
                        $("[name='sid']").val(obj);
                         },

                     })
                 $.ajax({
                                 url:"slist.do",
                                 type:"post",
                                 data:{"sid":sid},
                                 dataType:"json",
                                 success:function(obj) {
                                     for (var i in obj) {
                                   $("#tab").append("       <tr>\n" +
                                       "                  <td>"+obj[i].cname+"</td>\n" +
                                       "                  <td class='sc'>"+obj[i].score+"</td>\n" +
                                       "                  <td><input type='button' class='btn btn-danger' value='删除' onclick='todel("+obj[i].cid+")'>" +
                                       "<input type='button' class='btn btn-primary' value='修改' onclick='upd("+obj[i].cid+")'></td>\n" +
                                       "              </tr>")
                                     }
                                     mathScore();
                                 },

                             })
        function mathScore() {
         var num=0;
         var sum=0;
         $(".sc").each(function () {
            sum+=eval($("this")[0].innerText());
            num++;
         })
            $("[name='tosum']").val(sum);
            $("[name='tonum']").val(num);
            $("[name='toavg']").val(sum/num);
        }

    })
    function addStu() {
     let formData = new FormData($("#f1")[0]);
              $.ajax({
                              url:"upd.do",
                              type:"post",
                              data:"",
                              dataType:"json",
                              success:function() {

                              },
                                processData:false,
                           contentType:false,
                          })
    }
    function addSocre() {
                   $.ajax({
                                   url:"addcore.do",
                                   type:"post",
                                   data:$("#fom").serialize(),
                                   dataType:"text",
                                   success:function(obj) {
                                    if (obj!=null){
                                        alert("添加成功");
                                        location="toadd.do?snowId="+obj;
                                    }
                                   },

                               })
    }
</script>
</body>
</html>
