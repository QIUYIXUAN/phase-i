package com.qiu.bean;

public class Cla {
   private Integer id  ;
   private String name;
   private Integer pid ;
   private String  flag;

    public Cla() {
    }

    @Override
    public String toString() {
        return "Cla{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pid=" + pid +
                ", flag='" + flag + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public Cla(Integer id, String name, Integer pid, String flag) {
        this.id = id;
        this.name = name;
        this.pid = pid;
        this.flag = flag;
    }
}
