package com.qiu.bean;

public class Stu {
  private String  sid   ;
  private String  sname ;
  private Integer  lid   ;
  private String  lname ;
  private String  pic   ;
  private Integer  id    ;

    public Stu() {
    }

    @Override
    public String toString() {
        return "Stu{" +
                "sid='" + sid + '\'' +
                ", sname='" + sname + '\'' +
                ", lid=" + lid +
                ", lname='" + lname + '\'' +
                ", pic='" + pic + '\'' +
                ", id=" + id +
                '}';
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Integer getLid() {
        return lid;
    }

    public void setLid(Integer lid) {
        this.lid = lid;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Stu(String sid, String sname, Integer lid, String lname, String pic, Integer id) {
        this.sid = sid;
        this.sname = sname;
        this.lid = lid;
        this.lname = lname;
        this.pic = pic;
        this.id = id;
    }
}
