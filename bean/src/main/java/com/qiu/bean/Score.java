package com.qiu.bean;

public class Score {
   private Integer cid  ;
   private String cname;
   private Double score;
   private String sid  ;

    public Score() {
    }

    @Override
    public String toString() {
        return "Score{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", score=" + score +
                ", sid='" + sid + '\'' +
                '}';
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Score(Integer cid, String cname, Double score, String sid) {
        this.cid = cid;
        this.cname = cname;
        this.score = score;
        this.sid = sid;
    }
}
